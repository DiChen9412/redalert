const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './app/javascripts/app.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'app.js'
  },
  plugins: [
    // Copy our app's index.html to the build folder.
    new CopyWebpackPlugin([
      { from: './app/index.html', to: "index.html" }
    ])
  ],
  /*devServer: {
    host: "0.0.0.0",
    port: 8080
  },*/
  module: {
    rules: [
      {
       test: /\.css$/,
       use: [ 'style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpg|jpeg)$/,
        use: [
          {loader: 'url-loader', options: {limit: 8192}}
        ]
      }
    ],
    loaders: [
      { test: /\.json$/, use: 'json-loader' },
      { test: /\.html$/, loader: "html-loader" },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      },
      {
        test: /\.(png|jpg|jpeg)$/,
        use: [
          {loader: 'url-loader', options: {limit: 8192}}
        ]
      }
    ]
  }
}
