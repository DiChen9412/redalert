# README #

### What is this repository for? ###

This is a blockchain board game called blockchain redAlert. 

### How do I run it? ###
You need to install npm, nodejs, and truffle first (The newest version).
To play this game you should use Chrome browser with metamask injected which provides the account information for us to commit transactions.
After than, just type "npm run dev" in the command line under redalert folder and open "127.0.0.1:8080" in your Chrome.

### How does it look like? ###
You can get all you want by running the command above.