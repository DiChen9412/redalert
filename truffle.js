// Allows us to use ES6 in our migrations and tests.
require('babel-register')
require("babel-core/register");
require("babel-polyfill");

module.exports = {
  rpc: {
    host: 'localhost',
    port: '8545'
  },
  networks: {
    development: {
      //host: '127.0.0.1',
      host: 'localhost',
      port: 7545,
      network_id: '*' // Match any network id
    },
    rinkeby: {
      host: "localhost", // Connect to geth on the specified
      port: 8545,
      from: "0xEa4CE55D9aD70CA7D512374A73064fdc4933B388", // default address to use for any transaction Truffle makes during migrations
      network_id: 4,
      gas: 6712390 // Gas limit used for deploys
    }
  },
  solc: {
		optimizer: {
			enabled: true,
			runs: 200
		}
	},
}
