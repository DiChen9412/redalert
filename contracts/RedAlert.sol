pragma solidity ^0.4.2;

contract RedAlert {

    enum GameState { Created, SettingUp, Ready, Playing, Finished }

    struct Game {
        bytes32 gameID;
        address player1;
        address player2;
        string player1Name;
        string player2Name;
        address currentPlayer;
        address winner;
        GameState gameState;
        uint pot;
        uint availablePot;
        uint8 maxBoatLength;
        uint8 minBoatLength;
        uint timeStamp;
        //bytes32 hashesLength;
        //bytes32[] keywordsHashes;
        mapping(address => uint8) moveCount;
        mapping(address => string) encryptedGrids; 
        mapping(address => bytes32) roots;
        mapping(address => int8[10][10]) playerGrids;
        mapping(address => bool[4]) playerTanks;
    }

    mapping(address => string) public playerNames;
    mapping(address => string) public playerPublicKeys;
    mapping(bytes32 => bool) public playerNameExists;
    mapping(bytes32 => Game) public games;
    mapping(bytes32 => uint8) public hits1;
    mapping(bytes32 => uint8) public hits2;
    mapping(bytes32 => uint8) public Nready;
    mapping(address => bytes32[]) playerGames;
    mapping(address => string) public playerKeywords;
    mapping(address => bytes32[]) public playerKeywordsHash;
    mapping(address => bytes32) public hashLength;

    uint gameCount;
    uint i;
    uint j;
    uint cnt;
    uint8 Nchange;
    uint timeOutLim = 5 minutes;
    //uint8 public maxBoatLength;
    //uint8 public minBoatLength;

    // otherPlayerBoard
    int8[10][10] otherGrid;

    event PlayerSetName(address player, string name);
    event PlayerSetKeywords(address player , string keywords);
    event PlayerSetKeywordsHash(address player , bytes32[] hashes);
    event PlayerSetPublickKey(address player, string publicKey);

    event GameInitialized(bytes32 gameID, address player1, bool player1GoesFirst, uint pot);
    event GameJoined(bytes32 gameID, address player1, string player2Name);
    event TankPlaced(bytes32 gameID, address player, uint8 startX, uint8 endX, uint8 startY, uint8 endY);
    event StateChanged(bytes32 gameID, GameState newState, string newStateString);
    event BoardChanged(bytes32 gameID);

    event MadeMove(bytes32 gameID, address currentPlayer, address anotherPlayer,  uint8 x, uint8 y);
    event ConfirmedOthersMove(bytes32 gameID, address anotherPlayer,  uint8 x, uint8 y, uint8 hitNum);
    event SomeOneLied(bytes32 gameID, address liar, string liarName, bytes32 root,  bytes32 fakeRoot);
    event HitTank(bytes32 gameID, address currentPlayer, address anotherPlayer, uint8 x, uint8 y, int8 pieceHit);
    event WonChallenged(bytes32 gameID, address player);
    event GameEnded(bytes32 gameID, address winner, address loser);
    
    event WinningsWithdrawn(bytes32 gameID, address player);
    event WithdrawFailed(bytes32 gameID, address player, string reason);

    event IsStateCalled(bytes32 gameID, GameState currentState, GameState comparingState, bool equal);
    event IsPlayerCalled(bytes32 gameID, address player);
    event LogCurrentState(bytes32 gameID, GameState state);

    function getHashLength(address player) public returns (bytes32 result) {
        return hashLength[player];
    }

    function getPlayerKeywordsHash(address player, uint8 x) public returns (bytes32 result) {
        return playerKeywordsHash[player][x];
    }

    function stringToBytes32(string memory source) internal pure returns (bytes32 result) {
        assembly {
            result := mload(add(source, 32))
        }
    }

    modifier hasName() {
        if(stringToBytes32(playerNames[msg.sender]) != bytes32(0x0)) _;
    }

    modifier isPlayer(bytes32 gameID) {
        emit IsPlayerCalled(gameID,msg.sender);

        if(msg.sender == games[gameID].player1 || msg.sender == games[gameID].player2) _;
    }

    modifier isAnotherPlayer(bytes32 gameID) {
        if ((msg.sender == games[gameID].player1 || msg.sender == games[gameID].player2) && msg.sender !=games[gameID].currentPlayer) _;
    }

    modifier isCurrentPlayer(bytes32 gameID) {
        if(msg.sender == games[gameID].currentPlayer) _;
    }

    modifier isState(bytes32 gameID, GameState state){
        emit IsStateCalled(gameID, state, games[gameID].gameState, state == games[gameID].gameState);
        if(state == games[gameID].gameState) _;
    }

    function abs(int number) internal pure returns(uint unumber) {
        if(number < 0) return uint(-number);
        return uint(number);
    }


    function initialiseBoard(bytes32 gameID, address player) isState(gameID, GameState.Created) internal {
        for(uint8 i = 0; i < 10; ++i) {
            for(uint8 j = 0; j < 10; ++j) {
                games[gameID].playerGrids[player][i][j] = 0;
            }
        }
    }

    function findOtherPlayer(bytes32 gameID,address player) internal view returns(address) {
        if(player == games[gameID].player1) return games[gameID].player2;
        return games[gameID].player1;
    }

    function RedAlert() public {
        //maxBoatLength = 2;
        //minBoatLength = 2;
        gameCount = 0;
    }
    /*function existName(string name) public view returns(uint8) {
        bytes32 bytesname = stringToBytes32(name);
        if (playerNameExists[bytesname]) return 1;
        return 0;
    }*/
    function setName(string name, string publicKey, string keywords, bytes32[] hashs) public {
        require(bytes(name).length <= 30);
        require(bytes(publicKey).length <= 1000);
        require(bytes(keywords).length <= 1000 );

        bytes32 bytesname = stringToBytes32(name);
        //require(!playerNameExists[bytesname]);
        playerNames[msg.sender] = name;
        playerNameExists[bytesname] = true;

        emit PlayerSetName(msg.sender,name);

        playerPublicKeys[msg.sender] = publicKey;
        emit PlayerSetPublickKey(msg.sender, publicKey);

        playerKeywords[msg.sender] = keywords;
        /*playerKeywordsHash[msg.sender] = hashs;*/
        if (hashs.length>0)
        {
            for (uint8 i=0; i< hashs.length; ++i){
            playerKeywordsHash[msg.sender].push(hashs[i]);
            }
            hashLength[msg.sender] = bytes32(hashs.length);
            emit PlayerSetKeywords(msg.sender,keywords);
        }
    }
    
    function setKeywords(string keywords, bytes32[] hashs) public {
        require(bytes(keywords).length <= 1000 );
        playerKeywords[msg.sender] = keywords;
        /*playerKeywordsHash[msg.sender] = hashs;*/
        for (uint8 i=0; i< hashs.length; ++i){
            playerKeywordsHash[msg.sender].push(hashs[i]);
        }
        hashLength[msg.sender] = bytes32(hashs.length);
        emit PlayerSetKeywords(msg.sender,keywords);
    }

    function findPot(bytes32 gameID) internal view returns(uint){
        return games[gameID].pot;
    }

    function newGame(bool goFirst, uint8 maxLength, uint8 minLength) public hasName payable returns(bytes32){
        require(msg.value > 0);
        // Generate game id based on player's addresses and current block number
        gameCount += 1;
        bytes32 gameID = bytes32(gameCount);//keccak256(msg.sender, block.number);
        playerGames[msg.sender].push(gameID);
        games[gameID] = Game(
            gameID, //gameID
            msg.sender, // address player1;
            address(0), // address player2;
            playerNames[msg.sender], //     string player1Name;
            "",  // string player2Name;
            address(0), // address currentPlayer;
            address(0), // address winner;
            GameState.Created, // GameState gameState;
            msg.value * 2, // uint pot;
            msg.value * 2, // uint availablePot;
            maxLength,
            minLength,
            0
        );
        hits1[gameID] = 0;
        hits2[gameID] = 0;
        Nready[gameID] = 0;
        //for (uint8 i = 0; i < hashs.length; ++i)
        //    games[gameID].keywordsHashes.push(hashs[i]);
        if(goFirst){
            games[gameID].currentPlayer = msg.sender;
        }
        emit GameInitialized(gameID,msg.sender, goFirst,msg.value * 2);
        initialiseBoard(gameID,msg.sender);
        return gameID;
    }

    function joinGame(bytes32 gameID) public hasName isState(gameID, GameState.Created) payable { //join Others' game
        require(games[gameID].player2 == address(0)); //The game has a slot
        require(msg.value == games[gameID].pot / 2); //the bet
        games[gameID].player2 = msg.sender;
        games[gameID].player2Name = playerNames[msg.sender];
        playerGames[msg.sender].push(gameID);

        if(games[gameID].currentPlayer == address(0)){
            games[gameID].currentPlayer = msg.sender;
        }//this guy goes first 

        games[gameID].timeStamp=block.timestamp;

        initialiseBoard(gameID,msg.sender);
        emit GameJoined(gameID, games[gameID].player1, playerNames[msg.sender]);
        games[gameID].gameState = GameState.SettingUp;
        emit StateChanged(gameID,GameState.SettingUp,"SettingUp");
    }

    function showBoard(bytes32 gameID) public isPlayer(gameID) view returns(string) {
        return games[gameID].encryptedGrids[msg.sender];
    }

    function showOtherPlayerBoard(bytes32 gameID) isPlayer(gameID) public returns(int8[10][10]){
        require(games[gameID].gameState == GameState.Playing || games[gameID].gameState == GameState.Finished);
        address otherPlayer = findOtherPlayer(gameID,msg.sender);
        return games[gameID].playerGrids[otherPlayer];

    }

    function showCurrentPlayerBoard(bytes32 gameID) isPlayer(gameID) public returns(int8[10][10]){
        require(games[gameID].gameState == GameState.Playing || games[gameID].gameState == GameState.Finished);
        return games[gameID].playerGrids[msg.sender];
    }

    function shouldIConfirm(bytes32 gameID) isPlayer(gameID) public view returns(uint8) {
        return games[gameID].moveCount[findOtherPlayer(gameID, msg.sender)];
    }

    function getTimeStamp(bytes32 gameID)  isPlayer(gameID) public view returns(uint, uint) {
        return (games[gameID].timeStamp, block.timestamp);
    }

    function challengeTimeOut(bytes32 gameID)  isPlayer(gameID) public returns (int) {
        require(games[gameID].gameState == GameState.Playing || games[gameID].gameState == GameState.SettingUp);
        address otherPlayer = findOtherPlayer(gameID,msg.sender);
        if (games[gameID].gameState == GameState.Playing && 
            (((msg.sender == games[gameID].currentPlayer) && games[gameID].moveCount[findOtherPlayer(gameID, msg.sender)]!=0) ||
            (msg.sender != games[gameID].currentPlayer && games[gameID].moveCount[findOtherPlayer(gameID, msg.sender)]==0)) )
        {
            if (block.timestamp >= games[gameID].timeStamp + timeOutLim)
            {
                loseTheGame(gameID,otherPlayer);
                return int(block.timestamp)-int(games[gameID].timeStamp + timeOutLim);
            }
            else return int(block.timestamp)-int(games[gameID].timeStamp + timeOutLim);
        }
        if (games[gameID].gameState == GameState.SettingUp) 
        {
            if ((games[gameID].roots[otherPlayer][0]== 0) && block.timestamp >= games[gameID].timeStamp + timeOutLim)
            {
                loseTheGame(gameID,otherPlayer);
                return int(block.timestamp)-int(games[gameID].timeStamp + timeOutLim);
            }
            else if (games[gameID].roots[otherPlayer][0]!=0)
            {
                return 0;
            }
            else 
                return int(block.timestamp)-int(games[gameID].timeStamp + timeOutLim);
        }
        return 0;
    }
    /*function placeTank(bytes32 gameID, uint8 startX, uint8 endX, uint8 startY, uint8 endY) public isPlayer(gameID) isState(gameID,GameState.SettingUp) {
        
        require(startX == endX || startY == endY); //make sure it's a tank of size 1*n or n*1
        require(startX < endX || startY < endY);
        require(startX  < 10 && startX  >= 0 &&
                endX    < 10 && endX    >= 0 &&
                startY  < 10 && startY  >= 0 &&
                endY    < 10 && endY    >= 0); //check boundary 

        for(uint8 x = startX; x <= endX; x++) {
            for(uint8 y = startY; y <= endY; y++) {
                require(games[gameID].playerGrids[msg.sender][x][y] == 0);
            }   
        }
        uint8 boatLength = 1 + uint8(abs(int(startY) - int(endY))) + uint8(abs(int(startX) - int(endX)));

        require(boatLength <= games[gameID].maxBoatLength && boatLength >= games[gameID].minBoatLength); //the size of tanks should be in the range 
        require(!(games[gameID].playerTanks[msg.sender][boatLength])); //two tanks can not have the same size

        games[gameID].playerTanks[msg.sender][boatLength] = true;

        emit LogCurrentState(gameID,games[gameID].gameState);

        for(x = startX; x <= endX; x++) {
            for(y = startY; y <= endY; y++) {
                games[gameID].playerGrids[msg.sender][x][y] = int8(boatLength);
            }   
        }

        emit TankPlaced(gameID, msg.sender, startX, endX, startY, endY);
    }*/

    function readyToplay(bytes32 gameID, string encryptedGrids, bytes32 root) public isPlayer(gameID) isState(gameID,GameState.SettingUp) {
        require (games[gameID].roots[msg.sender][0]== 0);
        games[gameID].encryptedGrids[msg.sender]=encryptedGrids;
        games[gameID].roots[msg.sender]=root; 
        Nready[gameID]+=1;
        if (Nready[gameID] == 2) //two players are ready 
        {
            games[gameID].gameState = GameState.Playing;
            emit StateChanged(gameID,GameState.Playing,"Playing");
        }
        games[gameID].timeStamp=block.timestamp;
    }

    function changeBoard(bytes32 gameID, string encryptedGrids, bytes32 root) public isCurrentPlayer(gameID) isState(gameID,GameState.Playing) {
        games[gameID].encryptedGrids[msg.sender]=encryptedGrids;
        games[gameID].roots[msg.sender]=root;
        emit BoardChanged(gameID);
    }

    /*function finishPlacing(bytes32 gameID) public isPlayer(gameID) isState(gameID,GameState.Ready) {
        require ((bytes(games[gameID].encryptedGrids[games[gameID].player1]).length != 0) && 
            (bytes(games[gameID].encryptedGrids[games[gameID].player2]).length != 0));
        games[gameID].gameState = GameState.Playing;
        emit StateChanged(gameID,GameState.Playing,"Playing");
    }*/

    function makeMove(bytes32 gameID, uint8 x, uint8 y) public isState(gameID,GameState.Playing) isCurrentPlayer(gameID) {
        address otherPlayer = findOtherPlayer(gameID,msg.sender);
        require((games[gameID].playerGrids[otherPlayer][x][y] == 0 || games[gameID].playerGrids[otherPlayer][x][y] == 101) && 
        (games[gameID].moveCount[msg.sender]== 0 || games[gameID].moveCount[msg.sender]== x*10+y+1) );
        games[gameID].playerGrids[otherPlayer][x][y]=101;
        games[gameID].moveCount[msg.sender]=x*10+y+1;
        // 0: empty , min~max: unattacked tank , -(min~max): attacked tank  , 100: attacked empty grid
        games[gameID].timeStamp=block.timestamp;
        emit MadeMove(gameID, msg.sender, findOtherPlayer(gameID, msg.sender), x,y);
    }
    function checkProof(bytes proof, uint offset, uint len, bytes32 hash) internal view returns (bytes32) {
        bytes32 el;
        bytes32 h = hash;

        for (uint256 i = 32 + offset; i <= len +offset; i += 32) 
        {
            assembly 
            {
                el := mload(add(proof, i))
            }
            if (h < el) 
            {
                h = keccak256(h, el);
            }
            else 
            {
                h = keccak256(el, h);
            }
        }
        return h;
    }
    function loseTheGame(bytes32 gameID, address player){
        games[gameID].gameState = GameState.Finished;
        emit StateChanged(gameID,GameState.Finished,"Finished");
        games[gameID].winner = findOtherPlayer(gameID,player);
        emit GameEnded(gameID, findOtherPlayer(gameID,player), player);
    }
    function confirmOthersMove(bytes32 gameID, uint8 x, uint8 y, uint8 hitNum, bytes proof) public isState(gameID,GameState.Playing) isAnotherPlayer(gameID) {
        require (games[gameID].playerGrids[msg.sender][x][y] == 101);

        address otherPlayer=findOtherPlayer(gameID,msg.sender);
        bytes32 hash = keccak256(uint(hitNum)*100+uint(x)*10+uint(y));
        bytes32 root = checkProof(proof, 0, proof.length, hash);
        //bytes32 root2 = checkProof(proof, hash2);
        //emit SomeOneLied(gameID, otherPlayer, hash2, hash);
        bytes32 realRoot = games[gameID].roots[msg.sender];
        if (root != realRoot) 
        {
            emit SomeOneLied(gameID, msg.sender, playerNames[msg.sender], realRoot, root); //games[gameID].roots[otherPlayer]
            games[gameID].moveCount[otherPlayer]=0;
            games[gameID].playerGrids[msg.sender][x][y]=0;
            loseTheGame(gameID, msg.sender);
            return;
        }
        
        games[gameID].moveCount[otherPlayer]=0;
        // 0: nothit , 1: hit but not sure, -x: hit tank, max+1: miss
        if (hitNum == 0) 
        {
            games[gameID].playerGrids[msg.sender][x][y] = int8(100);
            games[gameID].currentPlayer = msg.sender;
            games[gameID].timeStamp=block.timestamp;
        }
        else 
        {
            emit HitTank(gameID, findOtherPlayer(gameID,msg.sender), msg.sender, x, y, int8(hitNum));
            games[gameID].playerGrids[msg.sender][x][y] = -1*int8(hitNum);
            if (msg.sender == games[gameID].player1) hits2[gameID]+=1;
            else hits1[gameID]+=1;
            games[gameID].timeStamp=block.timestamp;
        }
        emit ConfirmedOthersMove(gameID, msg.sender, x, y, hitNum);
        
    }

    function winningHits(bytes32 gameID) internal view returns(uint8){
        return (games[gameID].maxBoatLength+games[gameID].minBoatLength)*(games[gameID].maxBoatLength-games[gameID].minBoatLength +1)/2;
    }
    function different(bytes32 gameID, uint8 x, uint8 y) internal view returns(bool){
        uint8 mid = (games[gameID].minBoatLength + games[gameID].maxBoatLength) / 2;
        if (x<=mid && y> mid) return false;
        if (x> mid && y<=mid) return false;
        return true;
    }

    function sayWon(bytes32 gameID, uint[] len, bytes proof, uint8[] x, uint8[] y) public isPlayer(gameID) isState(gameID,GameState.Playing) {
        emit WonChallenged(gameID, msg.sender);

        cnt = 0;
        uint sum = 0;
        otherGrid = games[gameID].playerGrids[msg.sender];
        if (x.length!=winningHits(gameID) || y.length!=winningHits(gameID) || len.length!=winningHits(gameID)) 
        {
            emit SomeOneLied(gameID, msg.sender, playerNames[msg.sender], stringToBytes32("0"), stringToBytes32("0"));
            loseTheGame(gameID, msg.sender);
            return;
        }
        for (i = games[gameID].minBoatLength; i<= games[gameID].maxBoatLength; ++i)
        {
            Nchange=0;
            for (j = 0; j< i; ++j)
            {
                if (otherGrid[x[cnt]][y[cnt]]==0)
                    games[gameID].playerGrids[msg.sender][x[cnt]][y[cnt]]=int8(i);
                else 
                {
                    if ( different(gameID, uint8(-otherGrid[x[cnt]][y[cnt]]), uint8(i) ) )
                    {
                        emit SomeOneLied(gameID, msg.sender, playerNames[msg.sender], stringToBytes32("2"), stringToBytes32("2"));
                        loseTheGame(gameID, msg.sender);
                    }
                }

                otherGrid[x[cnt]][y[cnt]]=0;
                if (j>0 && x[cnt-1]!=x[cnt])
                {
                    Nchange = Nchange | 1;
                }
                if (j>0 && y[cnt-1]!=y[cnt])
                {
                    Nchange = Nchange | 2;
                }
                if (Nchange == 3 || (sum+len[cnt])>proof.length) // the size is not a line or outoflength
                {
                    emit SomeOneLied(gameID, msg.sender, playerNames[msg.sender], stringToBytes32("1"), stringToBytes32("1"));
                    loseTheGame(gameID, msg.sender);
                    return;
                }
                bytes32 root = checkProof(proof, sum ,len[cnt], keccak256(i*100+uint(x[cnt])*10+uint(y[cnt])));
                if (root != games[gameID].roots[msg.sender])
                {
                    emit SomeOneLied(gameID, msg.sender, playerNames[msg.sender], games[gameID].roots[msg.sender], root);
                    loseTheGame(gameID, msg.sender);
                    return;
                }
                sum +=len[cnt];
                cnt+=1;
            }
        }
        for( i = 0;  i < 10; i++) {
            for( j = 0;  j < 10; j++) {
                if(otherGrid[i][j] < 0){
                    emit SomeOneLied(gameID, msg.sender, playerNames[msg.sender], games[gameID].roots[msg.sender], root);
                    loseTheGame(gameID, msg.sender);
                    return;
                }
            }    
        }
        if((msg.sender == games[gameID].player2 ? hits2[gameID] : hits1[gameID]) == winningHits(gameID)) {
            loseTheGame(gameID, findOtherPlayer(gameID,msg.sender));
        }
    }

    function surrender(bytes32 gameID) public isPlayer(gameID) {
        loseTheGame(gameID, msg.sender);
    }

    function withdraw(bytes32 gameID) public payable {
        if(games[gameID].gameState != GameState.Finished){
            emit WithdrawFailed(gameID,msg.sender,'This game isnt over yet');
        }else{
            uint amount = games[gameID].availablePot;
            if(amount > 0){
                if(msg.sender == games[gameID].winner){
                    games[gameID].availablePot = 0;
                    msg.sender.transfer(amount);
                    emit WinningsWithdrawn(gameID, msg.sender);
                }else{
                    emit WithdrawFailed(gameID,msg.sender,'This player hasnt won the game');
                }
            }else{
                emit WithdrawFailed(gameID,msg.sender,'No more funds in the contract for this game');
            }
        }
    }
    function cancel(bytes32 gameID) public payable {
        if(games[gameID].gameState != GameState.Created){
            emit WithdrawFailed(gameID,msg.sender,'This game has started');
        }else{
            games[gameID].gameState = GameState.Finished;
            uint amount = games[gameID].availablePot/2;
            if(amount > 0){
                games[gameID].winner = msg.sender;
                games[gameID].availablePot = 0;
                msg.sender.transfer(amount);
                emit WinningsWithdrawn(gameID, msg.sender);
            }else{
                emit WithdrawFailed(gameID,msg.sender,'No more funds in the contract for this game');
            }
        }
    }
    
    /* This unnamed function is called whenever someone tries to send ether to the contract */
    function () {
        throw; // Prevents accidental sending of ether
    }
}
