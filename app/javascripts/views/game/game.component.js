import EthCrypto from 'eth-crypto';
//import sha3 from 'solidity-sha3';
//import { sha3withsize } from 'solidity-sha3';
import MerkleTree, { checkProof, merkleRoot, checkProofSolidityFactory } from 'merkle-tree-solidity'
import { sha3 } from 'ethereumjs-util'
var assert = require('assert');
var leftPad = require('left-pad')
//import 'web3';
//import { default as Web3} from 'web3';
var eventPool = {};// web3.eth.blockNumber;
function noExist(x)
  {
    if (!x) return true;
    if (x == null) return true;
    //console.log("Exist",x);
    return false;
  }
var countDown=0;
class Game {
  constructor($state,$timeout,RedAlert,Alert){
    this.RedAlert = RedAlert;
    this.Alert = Alert;
    this.$state = $state;
    this.$timeout = $timeout;
    this.gameID = this.$state.params.id;
    this.laying = {};
    this.setup();
    this.columns = ["A","B","C","D","E","F","G","H","I","J"];
    /*this.RedAlert.watch('GameJoined', async (err, result) => {
      let key = result.blockNumber + 'GameJoined';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
          eventPool[key]=1;
          console.log("gameJoined");
        if(result.args.player1 == this.RedAlert.data.account){
            this.Alert.add(`${result.args.player2Name} joined your current game!`); 
        }
      }
      });*/
    this.RedAlert.watch('SomeOneLied', async (err, result) => {
        let key = result.blockNumber + 'SomeOneLied';
        if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
          eventPool[key]=1;
          this.Alert.add(`${result.args.liarName} lied during the confirmation!`);
          console.log("Liar", result.args.liar);
          console.log("fb:realRoot", result.args.root);
          console.log("fb:fakeRoot", result.args.fakeRoot);
          if (result.args.liar != this.RedAlert.data.account)
          {
              this.canWin = true;
          }
        }
      });
    this.RedAlert.watch('MadeMove', async (err, result) => {
      let key = result.blockNumber + 'MadeMove';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
        eventPool[key]=1;
        console.log("Observe MadeMove");

        if(result.args.currentPlayer == this.RedAlert.data.account){
          await [this.getGameData(1),this.getBoard(),this.getOtherBoard()];
        }else
        {
          await this.confirmHit(parseInt(result.args.x), parseInt(result.args.y));
          await[this.getGameData(2),this.getBoard(),this.getOtherBoard()];
        }
      }
    });
    this.RedAlert.watch('ConfirmedOthersMove', async (err, result) => {
      let key = result.blockNumber + 'ConfirmedOthersMove';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
        eventPool[key]=1;
        console.log("Watch confirm:");
        if (result.args.hitNum == 0)
        {
            if(result.args.anotherPlayer == this.RedAlert.data.account){
                this.Alert.add(`Another player missed the missile! Now it's your turn!`);
            }else{
                this.Alert.add(`You missed a missile!`);
            }
        }
        await this.setCountDown();;
        await [this.getGameData(3),this.getBoard(),this.getOtherBoard()];
        this.moving = false;
      }
    });
    this.RedAlert.watch('StateChanged', async (err, result) => {
      let key = result.blockNumber + 'StateChanged';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
        eventPool[key]=1;
        try{
          await this.getGameData(4);
          console.log("stateChange:", this.data.gameState.toNumber());
          await this.getBoard();
          await this.getOtherBoard();
          if (this.data.gameState.toNumber() == 3)
          {
              this.Alert.add(`It's the time to start!`);
          }
          await this.setCountDown();;

        }catch(e){
          console.log("StateChanged Err");
        }
      }
    });
    this.RedAlert.watch('BoardChanged', async (err, result) => {
      let key = result.blockNumber + 'BoardChanged';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
        eventPool[key]=1;
        try{
          await this.getBoard();
          await this.getOtherBoard();
        }catch(e){
          console.log("BoardChanged Err");
        }
      }
    });
    this.RedAlert.watch('HitTank', async (err, result) => {
      let key = result.blockNumber + 'HitTank';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
        eventPool[key]=1;
        console.log("Hit Tank!");
        if(result.args.currentPlayer == this.RedAlert.data.account){
          //this.Alert.add(`You got a hit at ${this.columns[result.args.x]}${parseInt(result.args.y) + 1}! You hit a tank with length ${result.args.pieceHit.toNumber()}`);
          if (result.args.pieceHit.toNumber() <= this.midBoatLength){
            this.Alert.add(`You successfully hit a tank!`);
          } else {
            this.Alert.add(`You successfully killed a soldier!`);
          }
        }else
        {
          //this.Alert.add(`Your tank got hit at ${this.columns[result.args.x]}${parseInt(result.args.y) + 1}!`);
          if (result.args.pieceHit.toNumber() <= this.midBoatLength){
            this.Alert.add(`Your tank got hit!`);
          } else {
            this.Alert.add(`Your soldier was killed!`)
          }
        }
        await [this.getGameData(5),this.getBoard(),this.getOtherBoard()];
      }
    });
    this.RedAlert.watch('GameEnded', async (err, result) => {
      let key = result.blockNumber + 'GameEnded';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
        eventPool[key]=1;
        console.log("GameEnded");
        await [this.getGameData(5),this.getBoard(),this.getOtherBoard()];
        if(result.args.winner == this.RedAlert.data.account){
          this.Alert.add(`You're the winner of game#${this.RedAlert.bytes32ToInt(this.gameID)} :D`);
          this.data.winner = result.args.winner;
        }else if (result.args.loser == this.RedAlert.data.account)
        {
          this.Alert.add(`You're the loser of game#${this.RedAlert.bytes32ToInt(this.gameID)} :D`);
          this.data.winner = result.args.winner;
        }
      }
      await [this.getGameData(6),this.getBoard(),this.getOtherBoard()];
    });
    this.RedAlert.watch('WinningsWithdrawn', async (err, result) => {
      let key = result.blockNumber + 'WinningsWithdrawn';
      if(!eventPool[key] && this.loaded && result.args.gameID == this.gameID){
         eventPool[key]=1;
         await this.getGameData(7);
      }
    });
  }
  get myTurn(){
    return this.data && (this.RedAlert.data.account == this.data.currentPlayer);
  }
  async setup(){
    console.log("setup");
    await this.getGameData(8);
    await this.setCountDown();;
    //await this.getRedAlertDimensions();
    await this.getBoard();
    try{
      await this.getOtherBoard();
    }catch(e){
      console.log(e);
    }
    this.$timeout(() => this.loaded = true);
  }
  numberOfTanksPlaced(board){
    if(!board) return 0;
    let res=0;
    for (var i = 0; i < 10; ++i)
      for (var j = 0; j<10 ; ++j)
        if (board[i][j]!=0 && board[i][j]<100) res+=1;
    return res;
  }
  

  requirePrivateKey()
  {
      var PKname="__gamePrivateKey__"+this.RedAlert.data.account;
      console.log("Require privateKey");
      //console.log(this.RedAlert.privateKey, ",", sessionStorage.getItem(PKname));
      //console.log(PKname);
      if (noExist(this.RedAlert.privateKey) && noExist(sessionStorage.getItem(PKname)))
      {
          console.log("step1");
          this.RedAlert.privateKey = prompt("Please enter your privateKey for this game!");
          sessionStorage.setItem(PKname, this.RedAlert.privateKey);
      }
      else if (noExist(this.RedAlert.privateKey))
      {
          console.log("step2");
          this.RedAlert.privateKey = sessionStorage.getItem(PKname);
      }
      console.log("privateKey", this.RedAlert.privateKey);
  }
  get currentState(){
    if(this.data){
      //console.log(this.data.gameState.toNumber());
      return this.RedAlert.states[this.data.gameState.toNumber()];
    }
  }
  async gohome(){
    clearInterval(countDown);
    countDown=0;
    this.$state.go("home");
  }
  async refresh()
  {
      let gameID = this.gameID;
      await this.$state.go("home");
      await this.$state.go('game',{id: gameID});
  }
  async setCountDown()
  {
    let [TS,curT] =await this.RedAlert.call('getTimeStamp', [this.gameID]);
    console.log("timeStamps", ((TS-curT)+1800)*1000);
    console.log("res", TS, curT);
    let countDownDate=((TS-curT)+60*30)*1000;
    let myTurn = this.myTurn;
    if (this.data.gameState.toNumber() == 1) 
    {
        myTurn=true;
    }

    if (countDown)
    {
        console.log("exist CountDown!", countDown);
        clearInterval(countDown);
    }
    var now = new Date().getTime();
    console.log("countDownDate", countDownDate);
    countDownDate+=now;
    
    let self = this;
    countDown = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();
        self.countDown=1;
        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
        // Output the result in an element with id="demo"
        let timerN="timer2";
        if (myTurn) timerN="timer1";
        if (document.getElementById(timerN))
        document.getElementById(timerN).innerHTML = minutes + "mins " + seconds + "s ";
        //console.log(self);
        
        //console.log("distance", distance);
        //days + "d " + hours + "h "+ 
        
        // If the count down is over, write some text 
        if (distance < 0) {
            //clearInterval(countDown);
            document.getElementById(timerN).innerHTML = "Time Out!";
        }
    }, 1000);
    this.$timeout(() => {
      this.countDown = 1;
    }, 1000);
    console.log("new countDown", countDown, this.countDown);
    //this.countDown = countDown;
  }
  updateGameData(data)
  {
      for (var i = 0; i<this.RedAlert.data.games.length; ++i)
      if (this.RedAlert.data.games[i].gameID == this.gameID)
      {
          this.RedAlert.data.games[i]=data;
          break;
      }
  }
  async getGameData(idx=0){
    let data = await this.RedAlert.call('games',[this.gameID]);
    let res = await this.RedAlert.call('shouldIConfirm', [this.gameID]);
    this.updateGameData(data);
    //setCountDown(now+(1800)*1000);
    console.log("getGameData", idx, res, data);
    if(data.player1 == "0x0000000000000000000000000000000000000000")
      this.$state.go("home");
    this.data = data;
    this.getRedAlertDimensions();
    if (res>0)
    {
        let x=Math.floor((res-1)/10);
        let y=(res-1)%10;
        console.log("getGameData res",res);
        this.confirmHit(x,y);
    }

    this.$timeout(() => {
      this.data = data
      this.getRedAlertDimensions();
    });
  }
  async getRedAlertDimensions(){
    let [minBoatLength, maxBoatLength] = [this.data.minBoatLength, this.data.maxBoatLength];
    //console.log("Dimension", minBoatLength, maxBoatLength);
    // [await this.RedAlert.call('minBoatLength'),await this.RedAlert.call('maxBoatLength')];
    this.$timeout(() => {
      this.minBoatLength = minBoatLength.toNumber();
      this.maxBoatLength = maxBoatLength.toNumber();
      this.midBoatLength = (this.minBoatLength+ this.maxBoatLength)/2;
      this.tankHash=new Array(this.maxBoatLength - this.minBoatLength +1).fill(0);
    });
  }
  async getBoard(){
    let encryptedBoard = await this.RedAlert.call('showBoard',[this.gameID]);
    //console.log("getBoard", encryptedBoard);
    let board = new Array(10).fill(0).map(x => new Array(10).fill(0));
    let hitBoard = new Array(10).fill(0).map(x => new Array(10).fill(0));
    if (encryptedBoard == "") 
    {
      if (!this.board) 
      {
          console.log("No board");
          this.board = new Array(10).fill(0).map(x => new Array(10).fill(0));
          this.realTimeBoard=this.board
      }
      else 
      {
          console.log("Has board");
          this.realTimeBoard = this.board;
      }
      return;
    }
    else 
    {
      this.requirePrivateKey();
      try
      {
          let decryptedBoard = await EthCrypto.decryptWithPrivateKey(this.RedAlert.privateKey, JSON.parse(encryptedBoard));
          decryptedBoard = decryptedBoard.split(",");
          //console.log(decryptedBoard[1]);
          for (var i=0; i<10 ;++i)
            for (var j=0; j<10; ++j)
              board[i][j]=parseInt(decryptedBoard[i*10+j]);
          console.log("getBoard", board);
          this.ready = true;
          //console.log("this", this);
      }catch(e){
         this.Alert.add("Wrong privateKey!");
         var PKname="__gamePrivateKey__"+this.RedAlert.data.account;
         this.RedAlert.privateKey = null;
         sessionStorage.removeItem(PKname);
         this.requirePrivateKey();
         this.refresh();
      }
      try 
      {
           hitBoard = await this.RedAlert.call('showCurrentPlayerBoard', [this.gameID]);
           hitBoard = hitBoard.map((row) => row.map((ele) => ele.toNumber()));
      }
      catch(e)
      {

      }
      console.log("hitBoard-1", hitBoard);
      for (var i = 0 ;i< 10; ++i)
          for (var j = 0 ; j< 10; ++j)
              if (hitBoard[i][j] == 0 || hitBoard[i][j] ==101) hitBoard[i][j]=board[i][j];
      this.board = board;
      this.realTimeBoard=hitBoard;
      console.log("realTimeBoard", this.realTimeBoard);
    }
    /*board.map((row) => row.map((ele) => ele.toNumber()));
    var boardTranspose = board[0].map((col, i) => {
      return board.map((row) => row[i])
    });*/
    //console.log("hitBoard-2", hitBoard);
    this.$timeout(() => {
      this.board = board;
      this.realTimeBoard=hitBoard;
    });
  }
  async getOtherBoard(){
    let board = await this.RedAlert.call('showOtherPlayerBoard',[this.gameID]);
    board = board.map((row) => row.map((ele) => ele.toNumber()));

    this.$timeout(() => {
      this.otherBoard = board;
      if (this.moving)
      {
          console.log("decorate OtherBoard", this.x, this.y);
          this.otherBoard[this.x][this.y]=101;
      }
      console.log("getOtherBoard", this.otherBoard);
      this.canWin = this.ableToWin();
      if(!this.saidCanWin && this.canWin && this.data.availablePot.toNumber()) {
        this.Alert.add("You can win! Press the win button to tell the world!");
        this.saidCanWin = true;
      }
    });
  }
  
  async startGame(){
    try{
      await this.RedAlert.transaction('finishPlacing',[this.gameID]);
      await this.getGameData(9);
      await this.getBoard();
    }catch(e){
      this.Alert.add("Other player is still setting up their board");
    }
  }
  
  async reset()
  {
      this.board = new Array(10).fill(0).map(x => new Array(10).fill(0));
      this.realTimeBoard=this.board
      this.tankHash=new Array(this.maxBoatLength - this.minBoatLength +1).fill(0);
      this.ready = false;
      this.laying.x = null;
      this.laying.y = null;
      this.placed = false;
  }

  generateMerkleTree()
  {
      let eleList=[];
      for (var i=0; i<10 ;++i)
        for (var j=0; j<10; ++j)
        {
            let s=(this.board[i][j]*100+i*10+j).toString(16);
            eleList.push(sha3("0x"+leftPad(s, 64, 0)));
        }
      this.requirePrivateKey();
      for (var i=0; i<100; i++)
      {
          let nounce = this.RedAlert.privateKey+this.gameID+i.toString();
          eleList.push(sha3(nounce));
      }
      //console.log(eleList);
      return new MerkleTree(eleList);
  }
  uintArrayToString(uintArr)
  {
    let res=Buffer.from(uintArr).toString("hex");
    console.log("stringLen", res.length);
    return "0x"+res;
  }
  checkAllUnits(){
    for (var i = 0; i< this.maxBoatLength - this.minBoatLength+1; ++i)
    if( this.tankHash[i] ==0 ) {
        return false;
    }
    return true;
  }
  async readyToPlay(){
    //this.tankHash=new Array(this.maxBoatLength - this.minBoatLength +1).fill(0);
    if(!this.checkAllUnits()) {
        this.Alert.add("You have to set all your battle units before starting the game!");
        return;
    } 
    try{
      let stringBoard = "";
      for (var i=0; i<10 ;++i)
        for (var j=0; j<10; ++j)
          stringBoard +=this.board[i][j].toString()+",";
      let encryptedBoard = await EthCrypto.encryptWithPublicKey(this.RedAlert.publicKey, stringBoard);
      console.log("Board", this.board);
      console.log("Ready", encryptedBoard);
      let MKT= this.generateMerkleTree();
      let rt = MKT.getRoot();
      console.log("Root", rt);
      rt = this.uintArrayToString(rt);
      console.log("Root", rt);
      //console.log("gameID", this.gameID, typeof(this.gameID));
      this.ready = true;
      await this.RedAlert.transaction('readyToplay',[this.gameID, JSON.stringify(encryptedBoard), rt]);
      await this.getGameData(10);
      
    }catch(e){
      this.Alert.add("Something is wrong with your board!");
    }
  }
  async commitConfirmHit(x, y){
      try{
          console.log("confirmHit", x,y, this.board[x][y]);
          let MKT = this.generateMerkleTree();

          let s=(this.board[x][y]*100+x*10+y).toString(16);
          let hash = sha3("0x"+leftPad(s, 64, 0));
          //console.log("hash", this.uintArrayToString(hash));
          let proof = MKT.getProof(hash, x*10+y+1);

                //console.log("proof", proof, "\n", proof.length);

                //console.log("realRoot", this.uintArrayToString(MKT.getRoot()));
          await this.setCountDown();;
          await this.RedAlert.transaction('confirmOthersMove',[this.gameID, x, y, this.board[x][y], proof]);
          this.isConfirming=false;
      }catch(e)
      {
          this.Alert.add(`You didn't confirm the hit at (${x},${y})!`);
          this.isConfirming =  false;
      }
      
  }
  async confirmHit(x, y){
    if (this.isConfirming) return;
    this.isConfirming=true;
    await this.getBoard();
    let self = this;
    bootbox.confirm("Move has been placed, please confirm the hit!", 
      function(result){ 
          console.log('confirm callback: ' + result); 
          if (result)
          {
              self.commitConfirmHit(x, y);
          }
    });

    
  }
  ableToWin(){
    if(this.maxBoatLength && this.minBoatLength && this.otherBoard){
      let requiredToWin = (this.maxBoatLength+this.minBoatLength)*(this.maxBoatLength-this.minBoatLength+1)/2;
  
      let numberHit = this.otherBoard.reduce((c1, row) => {
        return row.reduce((c2,ele) => {
          if(ele < 0) c2 += 1;
          return c2;
        },c1);
      },0);
      return numberHit >= requiredToWin;
    };
    return false;
  }
  async winTheGame(){
    let num = new Array(10).fill(0);
    let xs = new Array(10).fill(0).map(x => new Array(10).fill(0));
    let ys = new Array(10).fill(0).map(x => new Array(10).fill(0));
    let MKT = this.generateMerkleTree();
    let proofs = "0x";
      //console.log("hash", this.uintArrayToString(hash));
      
    for (var i=0; i<10; ++i)
      for (var j=0;j<10; ++j)
      if (this.board[i][j]>0)
      {
          let k=this.board[i][j] - this.minBoatLength;
          xs[k][num[k]]=i;
          ys[k][num[k]]=j;
          num[k]++;
      }
    let cnt=0;
    let Xs=[]
    let Ys=[]
    let len=[];
    for (var i = this.minBoatLength; i <= this.maxBoatLength; ++i)
    for (var j = 0; j< i;j++)
    {
        let x=xs[i-this.minBoatLength][j];
        let y=ys[i-this.minBoatLength][j];
        Xs.push(x);
        Ys.push(y);
        cnt++;
        console.log("x", x ,"y", y, "hit", i);
        let s=(i*100+x*10+y).toString(16);
        let hash = sha3("0x"+leftPad(s, 64, 0));
        let proof = MKT.getProof(hash, x*10+y+1);
        console.log("proof",proof);
        proofs+=proof.substring(2);
        len.push((proof.length-2)/2);
    }
    console.log(len);
    console.log("proofs", proofs);
    let tx = await this.RedAlert.transaction('sayWon',[this.gameID, len, proofs, Xs, Ys]);
  }
  async withdrawWinnings(){
    if(!this.withdrawing){
      this.withdrawing = true;
      let tx = await this.RedAlert.transaction('withdraw',[this.gameID]);
    }
  }

  async commitSurrender()
  {
      try{
          await this.RedAlert.transaction('surrender',[this.gameID]);
      }
      catch(e)
      {
          this.Alert.add(e);
      }
  }
  async surrender(){
      let self = this;
      bootbox.confirm(
        "Are you sure to surrender?", 
        function(result){ 
          if (result)
          {
              self.commitSurrender();
          }
      });
  }

  async commitCancel(){
      try{
          let tx = await this.RedAlert.transaction('cancel',[this.gameID]);
          this.data.winner = this.RedAlert.data.account;
          this.getGameData(11);
      }
      catch(e)
      {
          this.Alert.add(e);
      }
  }
  async cancel(){
      let self = this;
      bootbox.confirm(
        "Are you sure to cancel this game?", 
        function(result){ 
          if (result)
          {
              self.commitCancel();
          }
      });
  }

  async commitChallenge()
  {
      try{
          await this.RedAlert.transaction('challengeTimeOut',[this.gameID]);
          this.data.winner = this.RedAlert.data.account;
      }
      catch(e)
      {
          this.Alert.add(e);
      }
  }
  async challengeTimeOut(){
    try{
        let res = await this.RedAlert.call('challengeTimeOut',[this.gameID]);
        console.log("challenge result", res);
        let self = this;
        if (res.toNumber() > 0){
            bootbox.confirm(
              "Another player may have a timeout! Do you want to challenge him?", 
              function(result){ 
                if (result)
                {
                    self.commitChallenge();
                }
            });
        }
        else if (res.toNumber() == 0)
        {
          this.Alert.add("It's your turn to act!");
        }
        else 
        {
          this.Alert.add(`It's too early to challenge! (${-res.toNumber()} seconds left)`);
        }
    }catch(e)
    {
        this.Alert.add(e);
    }

  }
  async makeMove(x,y){
    console.log("Try to makeMove",x,y);
    if(this.RedAlert.data.account != this.data.currentPlayer){
      this.Alert.add("it's not your turn, buddy");
      return;
    }
    if(!this.moving){
      this.moving = true;
      this.x = x;
      this.y = y;
      let tmp = 0;
      try{
        tmp = this.otherBoard[x][y];
        this.otherBoard[x][y]=101;
        await this.setCountDown();;
        let tx = await this.RedAlert.transaction('makeMove',[this.gameID,x,y]);

        console.log("madeMove",x,y);
        
        //console.log(tx);
      }catch(e){
        this.moving = false;
        this.otherBoard[x][y] = tmp;
        this.Alert.add("Move has not been placed");
        //this.getOtherBoard();
      }
    }
  }
  show()
  {
      this.getGameData(-1);
      //this.Alert.add(`${this.countDown}`);
  }
  inRange(x)
  {
    return x<10 && x>=0;
  }
  armyType(x)
  {
      let mid = this.midBoatLength;
      if (x <= mid) return 0;
      return 1;
  }
  async cheat()
  {
      this.cheating = true;

      this.tankHash=new Array(this.maxBoatLength - this.minBoatLength +1).fill(0);
      this.ready = false;
      this.laying.x = null;
      this.laying.y = null;
      this.placed = false;

      
      let hitBoard = await this.RedAlert.call('showCurrentPlayerBoard', [this.gameID]);
      hitBoard = hitBoard.map((row) => row.map((ele) => ele.toNumber()));

      this.$timeout(() => {
        this.board = hitBoard;
        this.realTimeBoard = hitBoard;
      });
  }

  async cancelCheat()
  {
      await this.getBoard();
      this.cheating = false;
  }

  async commitCheat()
  {
    try{
      let stringBoard = "";
      let backup = new Array(10).fill(0).map(x => new Array(10).fill(0));
      for (var i=0; i<10 ;++i)
        for (var j=0; j<10; ++j)
          {
              backup[i][j]=this.board[i][j];
              if (this.board[i][j] == 100) this.board[i][j]=0;
              if (this.board[i][j] < 0) this.board[i][j]*=-1;
              stringBoard +=this.board[i][j].toString()+",";
          }
      let encryptedBoard = await EthCrypto.encryptWithPublicKey(this.RedAlert.publicKey, stringBoard);

      console.log("Ready", encryptedBoard);
      let MKT= this.generateMerkleTree();
      let rt = MKT.getRoot();
      console.log("Root", rt);
      rt = this.uintArrayToString(rt);
      console.log("Root", rt);
      //console.log("gameID", this.gameID, typeof(this.gameID));
      this.ready = true;
      this.board=backup;
      this.realTimeBoard=backup;
      this.cheating = false;
      await this.RedAlert.transaction('changeBoard',[this.gameID, JSON.stringify(encryptedBoard), rt]);
      await this.getGameData(10);
      
    }catch(e){
      this.Alert.add("You canceled your change!");
    }
  }

  async confirmCheat()
  {
      if(!this.checkAllUnits()) {
        this.Alert.add("You have to set all your battle units before committing your new configuration!");
        return;
      } 
      if (this.numberOfTanksPlaced(this.board) > 
        (this.maxBoatLength+this.minBoatLength)*(this.maxBoatLength-this.minBoatLength+1)/2)
      {
        this.Alert.add("Your new configuration doesn't cover all dead battle units!");
        return;
      }
      let self = this;
      bootbox.confirm("Are you sure about your new configuration!", 
        function(result){ 
            console.log('confirm callback: ' + result); 
            if (result)
            {
                self.commitCheat();
            }
      });
  }


  placeTank(startX, endX, startY,endY)
  {
    if (this.data.gameState.toNumber() == 0) 
    {
        this.Alert.add("You have to wait for another player!");
        return;
    }
    if (this.data.gameState.toNumber() == 4) 
    {
        this.Alert.add("The game is end!");
        return;
    }
    console.log("PlaceTank", startX, endX, startY, endY);
    let len = Math.abs(startX-endX)+Math.abs(startY-endY)+1;
    if (!(startX == endX || startY == endY)) throw "The shape of your army should be a line";
    //assert((startX != endX || startY != endY));
    if (!(this.inRange(startX) && this.inRange(endX) && this.inRange(startY) && this.inRange(endY)))
      throw "Out of the boundary!";
    //the size of tanks should be in the range 
    if ((len > this.maxBoatLength || len < this.minBoatLength)) 
      throw `The shape of your army is out of the range [${this.minBoatLength},${this.maxBoatLength}] !`;
     //two tanks can not have the same size
    if ((this.tankHash[len - this.minBoatLength] !=0)) throw `Have you already laid an army of length ${len}?`;

    if (this.data.gameState.toNumber() == 1){ //normal setting up
      for (var i=startX; i<=endX; ++i)
        for (var j = startY; j<= endY; ++j)
          if (this.board[i][j] !=0)
            throw "You can't overlap other battle unit!";
    }
    else if (this.data.gameState.toNumber() == 3){ //cheating
      for (var i=startX; i<=endX; ++i)
        for (var j = startY; j<= endY; ++j){
          if (this.board[i][j]>=1 && this.board[i][j]<=10)
            throw "You can't overlap other battle unit!";

          if (this.board[i][j] == 100)
            throw "You can't change the facts that another player knows";

          if (this.board[i][j]<0 && this.armyType(-this.board[i][j])!= this.armyType(len) )
            throw "You can't change the facts that another player knows";
        }
    }
    this.tankHash[len-this.minBoatLength]=1;
    for (var i=startX; i<=endX; ++i)
        for (var j = startY; j<= endY; ++j)
            if (this.board[i][j]>=0)
            this.board[i][j]=len;

    if (this.data.gameState.toNumber() == 3)
      this.realTimeBoard = this.board;
    console.log("realTimeBoard", this.realTimeBoard);
  }
  async layPiece(x,y){
    console.log("layPiece:",x,y);
    //console.log("board", this.board);
    if(this.board[x][y] > 0) 
    {
        this.placed = false;  
        this.laying.x = null;
        this.laying.y = null;
        return;
    }
    if(this.placing) return;
    this.placing = true;
    try{
      if((this.minBoatLength>1 || this.tankHash[0]) && this.laying.x == x && this.laying.y == y){
        this.laying.x = null;
        this.laying.y = null;
        this.placed = false;
        console.log("condition1");
      }else if(this.placed){
        let startX = Math.min(this.laying.x,x);
        let startY = Math.min(this.laying.y,y);
        let endX = Math.max(this.laying.x,x);
        let endY = Math.max(this.laying.y,y);
        //await this.RedAlert.transaction('placeTank',[this.gameID,startX,endX,startY,endY]);
        console.log("condition2");
        this.placeTank(startX,endX,startY,endY);
        if (this.data.gameState.toNumber() == 1)
          await this.getBoard();
        this.placed = false;  
        this.laying.x = null;
        this.laying.y = null;
      }else{
        console.log("condition3");
        this.laying.x = x;
        this.laying.y = y;
        this.placed = true;
      }
    }catch(e){
      this.Alert.add(e);
      this.placed = false;  
      this.laying.x = null;
      this.laying.y = null;
      console.log(e);
    }
    this.placing = false;
  }

}

Game.$inject = ['$state','$timeout','RedAlert','Alert'];

let templateUrl = require('ngtemplate-loader!html-loader!./game.html');

export default {
  templateUrl: templateUrl,
  controller: Game,
  controllerAs: '$ctrl'
}
