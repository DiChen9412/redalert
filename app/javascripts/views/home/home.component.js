function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function bytes32ToInt(x){
    return parseInt(x);
  }

import EthCrypto from 'eth-crypto';
import { sha3 } from 'ethereumjs-util'
var eventPool = {};
function noExist(x)
  {
    if (!x) return true;
    if (x == null) return true;
    //console.log("Exist",x);
    return false;
  }
class Home {
  constructor($state, $timeout, RedAlert,Alert){
    this.RedAlert = RedAlert;
    this.Alert = Alert;
    this.$state = $state;
    this.$timeout = $timeout;
    this.setUp();
    this.RedAlert.watch('GameJoined', async (err, result) => {
      let key = result.blockNumber + 'HomeGameJoined';
      if(!eventPool[key] && this.loaded){
          eventPool[key]=1;
        if(result.args.player1 == this.RedAlert.data.account){
            this.Alert.add(`${result.args.player2Name} joined your game ${bytes32ToInt(result.args.gameID)}!`);
        }
      }
      });
    this.RedAlert.watch('GameInitialized', async (err, result) => {
      console.log("call otherAttackableGames");
      await this.otherAttackableGames();
      console.log("otherAttackableGames done.");
      });

    this.isHome = true;
    this.isAttack = false;
    this.isDefense = false;
    this.isJoined = false;
    this.isWatch = false;

    this.attackReady = false;
  };
  async setUp(){
    //await this.getGameData();
    /**/
    this.$timeout(() => {this.loaded = true;});
  }
  async getGameData(){
    let data = await this.RedAlert.call('games',[1]);
    console.log(data);
    if(data.player1 == "0x0000000000000000000000000000000000000000")
      this.$state.go("home");
    this.$timeout(() => this.data = data);
  }
  /*async setUpWatch() {
    await this.loaded.promise;
    let instance = await RedAlert.deployed();

    instance
    .PlayerSetName({},{fromBlock: 0, toBlock: 'pending'})
    .watch(async (err, result) => {
      let name = await instance.playerNames.call(this.data.account);
      this.$timeout(() => this.name = name);
    });
  }*/

  async commitNewGame(goFirst, maxLength, minLength, eth){
      let result = await this.RedAlert.transaction('newGame',[goFirst, maxLength, minLength],{value: eth});
  }
  async newGame(){
    try{
      if(!this.RedAlert.name) throw "You have to set your name first!";  
      if(!this.RedAlert.publicKey) throw "You have to set your game Keys!";
      let self = this;
      bootbox2.form({
          title: 'Game Setting',
          fields: {
              eth: {
                  label: '#eth',
                  type:  'number',
                  placeholder: 'positive integer'
              },
              minLength: {
                  label: 'minimal length',
                  type:  'number',
                  placeholder: '1-10'
              },
              maxLength: {
                  label: 'maximal length',
                  type:  'number',
                  placeholder: '1-10'
              },
          },
          callback: function (values) {
              if (!values) return;
              console.log("prompt call back", values);
              let minLength = 0;
              let maxLength = 0;
              let goFirst = getRandomInt(2) == 0 ? true : false;
              //let value = prompt("How many ETH are you putting into the pot? (integer)");
              //let range = prompt("What's the range of the size of tanks? [a,b] 2<=a<=b<=10");
              minLength = parseInt(values["minLength"]);
              maxLength = parseInt(values["maxLength"]);
              let value = parseInt(values["eth"]);
              /*let substrings = range.split(",");
              if (substrings.length >2) throw "Illegal size range!";
              let stra = substrings[0];
              let strb = substrings[1];

              for (var i=0; i< stra.length; ++i)
                if (stra[i]<='9' && stra[i] >='0')
                {
                  minLength=minLength*10+parseInt(stra[i]);
                }

              for (var i=0; i< strb.length; ++i)
                if (strb[i]<='9' && strb[i] >='0')
                {
                  maxLength=maxLength*10+parseInt(strb[i]);
                }*/
              if ((minLength > maxLength) || (minLength <=0) || (maxLength>10)) throw "Illegal size range!";
              //value = parseInt(value);
              if(isNaN(value)) throw "Please enter a number!";
              if(value <= 0) throw "The minimum bet should be at least one ether!";
              let eth = parseInt(value*Math.pow(10,18));  
              self.commitNewGame(goFirst, maxLength, minLength, eth);
          }
      });
    }catch(e){
      this.Alert.add(e)
    }
  }
  async commitSetName(name, publicKey, keywords){
      /*let exists = await this.RedAlert.call('existName', name);
      console.log("nameExists", exists);
      if (exists.toNumber()==1)
      {
          this.Alert.add("That name has already been taken by another player");
          return;
      }*/
      try{
          this.RedAlert.name = name;
          let res = await this.parseKeywords(keywords);
          let c = "";
          let d = [];
          if (res) 
          {
             c = res[0];
             d = res[1];
          } 
          let result = await this.RedAlert.transaction('setName',[name, publicKey, c, d]);
          console.log("commitSetName", result);
          bootbox.alert({
              title: "Please save your privateKey to a safe place",
              message: `${this.RedAlert.privateKey}`,
              callback: function () {
                  console.log('This was logged in the callback!');
              }
          });

          this.Alert.add();
      }
      catch(e)
      {
          //this.Alert.add("You canceled the sign up!");
          location.reload();
      }
            
  }
  async setName() {
      let self = this;
      console.log("call setName");
      bootbox2.form({
          title: 'Sign Up',
          fields: {
              name: {
                  label: 'username',
                  type:  'text',
                  placeholder: 'at most 32 characters'
              },
              keywords: {
                  label: 'keywords',
                  type:  'text',
                  placeholder: 'at most 5 keywords each'
              }
          },
          callback: function (values) {
              if (!values) return;
              console.log("values");
              let name = values["name"];
              let keywords = values["keywords"];
              if(!name) return;
              name = name.substring(0,32);
              try{
                  //self.Alert.add("Generating your gameKeys and registering!");
                  let identity = EthCrypto.createIdentity();
                  console.log("publicKey: ", identity["publicKey"]);
                  console.log("privateKey: ",identity["privateKey"]);

                  
                  self.RedAlert.publicKey = identity["publicKey"];
                  self.RedAlert.privateKey = identity["privateKey"];

                  var PKname="__gamePrivateKey__"+self.RedAlert.data.account;
                  sessionStorage.setItem(PKname, self.RedAlert.privateKey);
                  console.log("publicKey: ", identity["publicKey"]);
                  console.log("privateKey: ",identity["privateKey"]);

                  self.commitSetName(name, identity["publicKey"], keywords);
              }catch(e){
                self.Alert.add("That name has already been taken by another player");
                return ;
              }
          }
    });
  }
  requirePrivateKey()
  {
      var PKname="__gamePrivateKey__"+this.RedAlert.data.account;
      console.log("Require privateKey");
      //console.log(this.RedAlert.privateKey, ",", sessionStorage.getItem(PKname));
      //console.log(PKname);
      if (noExist(this.RedAlert.privateKey) && noExist(sessionStorage.getItem(PKname)))
      {
          console.log("step1");
          this.RedAlert.privateKey = prompt("Please enter your privateKey for this game!");
          sessionStorage.setItem(PKname, this.RedAlert.privateKey);
      }
      else if (noExist(this.RedAlert.privateKey))
      {
          console.log("step2");
          this.RedAlert.privateKey = sessionStorage.getItem(PKname);
      }
  }
  uintArrayToString(uintArr)
  {
      let res=Buffer.from(uintArr).toString("hex");
      console.log("stringLen", res.length);
      return "0x"+res;
  }
  async parseKeywords(keywords){
      //keywords = keywords.toLowerCase();
      keywords = keywords.split(" ");
      var flag = true;
      var fliteredKeywords = "";
      let hashKeywords = [];
      console.log(keywords);
      var cnt = 0;
      //this.RedAlert.keywordsHashes = [];
      for (var i = 0; i < keywords.length && cnt < 5 ; ++i) 
      {
          if (keywords[i]!="")
          {
              flag = false;
              cnt ++; 
              let s=keywords[i].substring(0,32);
              fliteredKeywords+=(cnt == 1 ? "": " ")+s;
              hashKeywords.push( this.uintArrayToString(sha3(s.toLowerCase() ) ) );
              //this.RedAlert.keywordsHashes.push(this.uintArrayToString(sha3(s)));
          }
      }
      if(flag) 
        return false;
      console.log(hashKeywords);
      if (!this.RedAlert.publicKey) 
        this.RedAlert.publicKey=await this.RedAlert.call('playerPublicKeys',[this.RedAlert.data.account]);
      //this.requirePrivateKey();
      console.log("publicKey", this.RedAlert.publicKey);
      let encryptedKeywords = await EthCrypto.encryptWithPublicKey(this.RedAlert.publicKey, fliteredKeywords);
      this.RedAlert.keywords = fliteredKeywords;
      console.log("Set keywords", fliteredKeywords);

      return [JSON.stringify(encryptedKeywords), hashKeywords];

  }
  async commitSetKeywords(keywords){
      let res = await this.parseKeywords(keywords);
      console.log("parseKeywords", res);
      if (res){
          try{
            let result = await this.RedAlert.transaction('setKeywords',[res[0], res[1]]);
          }catch(e){
            this.Alert.add(e);
            this.Alert.add("Something is wrong with your keywords!");
          }
      }
  }
  async setKeywords() {
    try{
      if(!this.RedAlert.name) throw "You have to set your name first!";
    }catch(e){
      this.Alert.add(e);
      return;
    }
    let self = this;
    bootbox.prompt({
      title: "Reset your keywords",
      inputType: 'textarea',
      placeholder: "at most 5 keywords; at most 32 characters each; seperated by space;",
      callback: function (keywords) {
          if (!keywords) return;
          console.log(keywords);
          self.commitSetKeywords(keywords);
      }
    });
  }
  async commitJoinGame(game)
  {
      let amountToBet = game.pot.toNumber() / 2;
      let result = await this.RedAlert.transaction('joinGame',[game.gameID],{value: amountToBet});
      await this.$state.go('game',{id: game.gameID});
  }
  async joinGame(game){
    
    let amountToBet = game.pot.toNumber() / 2;
    console.log("GamePot", game.pot, amountToBet);
    
    let self = this;
    try{
        if(!this.RedAlert.name) throw "Make sure your name is set to join a game";
        if(!this.RedAlert.publicKey) throw "You have to set your game Keys first!";

        bootbox.confirm(`Do you want to join this game for ${this.RedAlert.weiToEth(amountToBet)} ETH?`, 
          function(result){ 
              console.log('confirm callback: ' + result); 
              if (result)
              {
                  self.commitJoinGame(game);
              }
        });
    }catch(e){
      this.Alert.add(e);
    }
  }
  async playGame(game){
    //console.log("playGame", game);
    this.$state.go('game',{id: game.gameID});
  }

  async goToHome() {
    this.isHome = true;
    this.isAttack = false;
    this.isDefense = false;
    this.isJoined = false;
    this.isWatch = false;
  }

  async goToAttack() {
    console.log("to start otherAttackableGames func");
    await this.otherAttackableGames();
    console.log("done with otherAttackableGames func");
    this.isHome = false;
    this.isAttack = true;
    this.isDefense = false;
    this.isJoined = false;
    this.isWatch = false;
    //this.$timeout(() => this.attackReady = true);
  }

  async goToDefense() {
    this.isHome = false;
    this.isAttack = false;
    this.isDefense = true;
    this.isJoined = false;
    this.isWatch = false;
  }

  async goToJoined() {
    this.isHome = false;
    this.isAttack = false;
    this.isDefense = false;
    this.isJoined = true;
    this.isWatch = false;
  }

  async goToWatch() {
    this.isHome = false;
    this.isAttack = false;
    this.isDefense = false;
    this.isJoined = false;
    this.isWatch = true;
  }

  async otherAttackableGames(){
    console.log("inside otherAttackableGames func");
    let gamesList = this.RedAlert.data.games.filter((e) =>e.winner ==0 && e.player1 != this.RedAlert.data.account && e.player2 == 0);
    let gameids = [];
    let matchedHashCnts = [];
    let orders = [];

    console.log("account:",this.RedAlert.data.account);
    //let myHashes = this.RedAlert.keywordsHashes;
    let myHashLen = await this.RedAlert.call('getHashLength', [this.RedAlert.data.account]);
    console.log("myHashLen:", myHashLen);
    let myHashes = [];
    for (var i = 0; i < myHashLen; ++i) {
      myHashes.push(await this.RedAlert.call('getPlayerKeywordsHash', [this.RedAlert.data.account, i]));
    }
    console.log("get myHashes:", myHashes);

    for (var i = 0; i < gamesList.length; ++i) {
      let e = gamesList[i];
      gameids.push(e.gameID);
      let otherHashes = [];
      let otherHashLen = await this.RedAlert.call('getHashLength', [e.player1]);
      //let hashlen = 3;
      console.log("otherHashLen:", otherHashLen);
      for (var j = 0; j < otherHashLen; j++)
        otherHashes.push(await this.RedAlert.call('getPlayerKeywordsHash', [e.player1, j]));
      console.log("otherHashes:", otherHashes);
      let sameCnt = 0;
      for (var j = 0; j < myHashes.length; ++j) {
        let isMatched = false;
        for (var k = 0; k < otherHashes.length; ++k) {
          if (otherHashes[k] == myHashes[j]) {
            isMatched = true;
            break;
          }
        }
        if (isMatched) sameCnt++;
      }
      matchedHashCnts.push(sameCnt);
      orders.push(i);
    }
    console.log("gameids:", gameids);
    console.log("matchedHashCnts:", matchedHashCnts);
    console.log("orders:", orders);

    orders.sort( function(x1, x2) {
      if (matchedHashCnts[x1] > matchedHashCnts[x2]) return -1;
      else if (matchedHashCnts[x1] < matchedHashCnts[x2]) return 1;
      else {
        if (gameids[x1] < gameids[x2]) return -1;
        else return 1;
      }
    });

    console.log("orders:", orders);
    let sortedList = [];
    for (var i = 0; i < orders.length; ++i)
      sortedList.push(gamesList[orders[i]]);
    this.$timeout(() => this.AttackableGames = sortedList);
    //return sortedList;
  }

  requirePrivateKey()
  {
      console.log("Require privateKey", this.RedAlert.privateKey);
      if (!this.RedAlert.privateKey)
      this.RedAlert.privateKey = prompt("Please enter your privateKey for this game!");
  }
  get myGames(){
    let gamesList = this.RedAlert.data.games.filter((e) => (e.player1 == this.RedAlert.data.account && (e.player2!=0 || e.winner==0) ) );
    let orders = [];
    let gameids = [];
    for (var i = 0; i < gamesList.length; ++i) {
      orders.push(i);
      gameids.push(gamesList[i].gameID);
      
    }
    //console.log(orders);
    orders.sort(
      function(x1, x2) {
        if (gameids[x1] < gameids[x2]) return -1;
        else return 1;
    });
    let sortedList = [];
    for (var i = 0; i < orders.length; ++i)
    {
      var t = gamesList[orders[i]];
      sortedList.push(t);
      //console.log(this.RedAlert.bytes32ToInt(t.gameID));
    }
    //console.log("\n");
    return sortedList;
  }

  get joinedGames(){
    return this.RedAlert.data.games.filter((e) => e.player2 == this.RedAlert.data.account);
  }

  get otherUnattackableGames(){
    return this.RedAlert.data.games.filter((e) => e.player1 != this.RedAlert.data.account && e.player1 != 0 && e.player2 !=0 && e.player2 != this.RedAlert.data.account);
  }
}

Home.$inject = ['$state','$timeout','RedAlert','Alert'];

let templateUrl = require('ngtemplate-loader!html-loader!./home.html');

export default {
  templateUrl: templateUrl,
  controller: Home,
  controllerAs: '$ctrl'
}
