// Import libraries we need.
import 'web3';
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract';
import EthCrypto from 'eth-crypto';
// Import our contract artifacts and turn them into usable abstractions.
import RedAlertDef from '../../../build/contracts/RedAlert.json'

var RedAlert = contract(RedAlertDef);

// give it web3 powers!
RedAlert.setProvider(web3.currentProvider);
function noExist(x)
  {
    if (!x) return true;
    if (x == null) return true;
    if (x == "null") return true;
    console.log("Exist",x);
    return false;
  }
class RedAlertService {

	constructor($q,$timeout) {
		this.$timeout = $timeout

		this.loaded = $q.defer();
		this.keywords = "";
		this.data = {
			games: []
		};

		web3.eth.getAccounts((err,accs) => {
			if (err != null) {
				alert("There was an error fetching your accounts.");
				return;
			}

			if (accs.length == 0) {
				alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
				return;
			}

			this.accounts = accs;
			angular.extend(this.data,{account: this.accounts[0]});
			//angular.extend(this.data,{publicKey: accs});
			this.loaded.resolve();
			this.setUpWatch();
			//this.initialization();
		});

		this.states = ['Created', 'SettingUp', 'Ready',  'Playing', 'Finished'];
	}
	requirePrivateKey()
	  {
	      var PKname="__gamePrivateKey__"+this.data.account;
	      console.log("Require privateKey");
	      //console.log("storage 1", sessionStorage.getItem(PKname));
	      //console.log("storage 2", this.privateKey); 
	      if (noExist(this.privateKey) && noExist(sessionStorage.getItem(PKname)))
	      {
	          console.log("step1");
	          this.privateKey = prompt("Please enter your privateKey for this game!");
	          sessionStorage.setItem(PKname, this.privateKey);
	      }
	      else if (noExist(this.privateKey))
	      {
	          console.log("step2");
	          this.privateKey = sessionStorage.getItem(PKname);
	      }
	      console.log("privateKey", this.privateKey);
	  }

	weiToEth(wei){
		return parseInt(wei/Math.pow(10,18));
	}

	ethToWei(eth){
		return parseInt(eth*Math.pow(10,18));
	}

	bytes32ToInt(x){
		return parseInt(x);
	}
	
	async transaction(method, args=[], vars={value: 0}) {
		await this.loaded.promise;
		let instance = await RedAlert.deployed();
		angular.extend(vars,{from: this.data.account, gas: 2000000});
		return await instance[method](...args,vars);
	}

	async call(attribute,args=[],vars={}) {
		await this.loaded.promise;
		let instance = await RedAlert.deployed();
		angular.extend(vars,{from: this.data.account});
		//console.log(attribute,...args,vars);
		let result = await instance[attribute].call(...args,vars);
		if(attribute == 'games') result = this.structToObject(result);

		return result;
	}

	async watch(Name, cb) {
		await this.loaded.promise;
		let instance = await RedAlert.deployed();
		instance[Name]({},{toBlock: 'pending'})
		.watch(cb);
	}
	async initialization() {
		console.log("account", this.data.accout);
		try{
			let name = await instance.playerNames.call(this.data.account);
			this.name = name;
			console.log("name", name);
		}catch(e){console.log("No Name!");}

		try{
			let publicKey = await instance.playerPublicKeys.call(this.data.account);
			this.publicKey = publicKey;
			console.log("publicKey", publicKey);
		}catch(e){
			console.log("no publicKey");
		}

		try{
			let encryptedKeywords = await instance.playerKeywords.call(this.data.account);
			console.log("encryptedKeywords", encryptedKeywords);
			let keywords ="";
			if (encryptedKeywords != "") 
			{
				this.requirePrivateKey();
				keywords= await EthCrypto.decryptWithPrivateKey(this.privateKey, JSON.parse(encryptedKeywords));
			}
			console.log("findKeywords", keywords);
			this.keywords = keywords;
			console.log("keywords", keywords);
		}catch(e)
		{
			console.log("no Keywords");
		}
		

		
	}

	async setUpWatch() {
		await this.loaded.promise;
		let instance = await RedAlert.deployed();

		console.log("EthCrypto", EthCrypto);
		instance
		.PlayerSetName({},{fromBlock: 0, toBlock: 'latest'})
		.watch(async (err, result) => {
			if (result.args.player != this.data.account) return ;
			let name = await instance.playerNames.call(this.data.account);
			this.$timeout(() => this.name = name);
			console.log("get Name", name);
		});

		instance
		.PlayerSetPublickKey({},{fromBlock: 0, toBlock: 'latest'})
		.watch(async (err, result) => {
			if (result.args.player != this.data.account) return ;
			let publicKey = await instance.playerPublicKeys.call(this.data.account);
			this.$timeout(() => this.publicKey = publicKey);
			console.log("get publicKey", publicKey);
		});

		instance
		.PlayerSetKeywords({},{fromBlock: 0, toBlock: 'latest'})
		.watch(async (err, result) => {
			if (result.args.player != this.data.account) return ;
			let encryptedKeywords = await instance.playerKeywords.call(this.data.account);
			console.log("encryptedKeywords", encryptedKeywords);
			let keywords ="";
			if (encryptedKeywords != "") 
			{
				this.requirePrivateKey();
				try
				{
					keywords= await EthCrypto.decryptWithPrivateKey(this.privateKey, JSON.parse(encryptedKeywords));
				}catch(e)
				{
					console.log("wrong privateKey!");
				}
			}
			console.log("findKeywords", keywords);
			this.$timeout(() => this.keywords = keywords);

		});

		

		instance
		.GameInitialized({},{fromBlock: 0, toBlock: 'latest'})
		.watch(async (err, result) => {
			let game = await instance.games.call(result.args.gameID);
			game = this.structToObject(game);
			//game.id = result.args.gameID;
			this.$timeout(() => {
				this.data.games = this.data.games.filter((_game) => _game.gameID != game.gameID);
				this.data.games.push(game);
				console.log("watched new game",this.data.games);
			});
		});

		instance
		.GameJoined({},{fromBlock: 0, toBlock: 'latest'})
		.watch(async (err, result) => {
			let game = await instance.games.call(result.args.gameID);
			game = this.structToObject(game);
			//game.id = result.args.gameID;
			this.$timeout(() => {
				this.data.games = this.data.games.filter((_game) => _game.gameID != game.gameID);
				this.data.games.push(game);
				console.log("watched joined game",this.data.games);
			});
		});
	}

	structToObject(game){
		let result = [
			"gameID",
			"player1",
			"player2",
			"player1Name",
			"player2Name",
			"currentPlayer",
			"winner",
			"gameState",
			"pot",
			"availablePot",
			"maxBoatLength",
			"minBoatLength"
		].reduce((c,key,index) => {
			c[key] = game[index];
			return c;
		},{});
		return result;
	}
}

RedAlertService.$inject = ['$q','$timeout'];

export default RedAlertService;